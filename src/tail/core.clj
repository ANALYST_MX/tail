(ns tail.core
  (:require [clojure.tools.cli :refer [parse-opts]]
            [clojure.string :as string]
            [clojure.java.io :as io])
  (:import [java.io RandomAccessFile]))

(defn join [coll]
  (string/join \newline coll))

(def cli-options
  [
   ["-f" nil "Loop forever trying to read more characters at the end of the file, presumably because the file is growing."
    :id :follow
    :default false]
   ["-n" "--lines COUNT" "Output the last COUNT lines."
    :default 10
    :parse-fn #(Integer/parseInt %)]
   ["-h" "--help"]])

(defn usage [options-summary]
  (->> ["Tail prints the last part (10 lines by default) of file."
        ""
        "Usage: tail [options] file"
        ""
        "Options:"
        options-summary
        ""
        "Please refer to the manual page for more information."]
       join))

(defn error-msg [errors]
  (str "The following errors occurred while parsing your command:\n\n"
       (string/join \newline errors)))

(defn exit [status msg]
  (println msg)
  (System/exit status))

(defn raf-seq [#^RandomAccessFile raf]
  (if-let [line (.readLine raf)]
    (lazy-seq (cons line (raf-seq raf)))
    (do (Thread/sleep 500)
        (recur raf))))

(defn tail-seq [path]
  (let [raf (RandomAccessFile. (io/file path) "r")]
    (.seek raf (.length raf))
    (raf-seq raf)))

(defn last-lines [path n]
  (join (take-last n (line-seq (io/reader path)))))

(defn loop-lines [path f]
  (doseq [line (tail-seq path)] (f line)))

(defn -main [& args]
  (let [{:keys [options arguments errors summary]} (parse-opts args cli-options)
        f println]
    (cond
      (:help options) (exit 0 (usage summary))
      (not= (count arguments) 1) (exit 1 (usage summary))
      errors (exit 1 (error-msg errors)))
    (f (last-lines (first arguments) (:lines options)))
    (when (:follow options)
      (loop-lines (first arguments) f))))
