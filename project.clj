(defproject tail "0.1.0-SNAPSHOT"
  :description "Tail prints the last part (10 lines by default) of file."
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.6.0"]
                 [org.clojure/tools.cli "0.3.1"]]
  :main tail.core)
